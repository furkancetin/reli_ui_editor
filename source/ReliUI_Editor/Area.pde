class Area {
  float x;
  float y;
  float w;
  float h;

  public boolean isMouseHover        = false;
  public boolean isMouseEnter        = false;
  public boolean isMouseLeave        = false;

  public boolean isMousePressing     = false;
  public boolean isMousePressed      = false;
  public boolean isMouseReleased     = false;
  public boolean isMouseDragging     = false;
  public boolean isSelectable        = true;
  public boolean isSelected          = false;

  protected boolean isMouseHoverPre     = false;

  Area() {
    x = 0;
    y = 0;
    w = 10;
    h = 10;
  }

  Area(float _x, float _y, float _w, float _h) {
    x = _x;
    y = _y;
    w = _w;
    h = _h;
  }

  public void setArea(float _x, float _y, float _w, float _h) {
    x = _x;
    y = _y;
    w = _w;
    h = _h;
  }

  public void setArea(Area _area) {
    x = _area.x;
    y = _area.y;
    w = _area.w;
    h = _area.h;
  }

  //Used for debugging
  public void renderArea() {
    stroke(120);
    strokeWeight(1);
    noFill();
    rect(x, y, w, h);
  }

  public void updateArea() {
    isMouseHoverPre = isMouseHover;
    isMouseHover = isInside(mouseX, mouseY);
    isMouseEnter = !isMouseHoverPre && isMouseHover;
    isMouseLeave =  isMouseHoverPre && !isMouseHover;

    isMousePressing = mousePressed && isMouseHover;
    
    isMouseDragging = isMousePressing && ((mouseX != pmouseX) || (mouseY != pmouseY));
    isMousePressed  = isMouseHover && isGlobalMousePressed;
    isMouseReleased = isMouseHover && isGlobalMouseReleased;

    if (isSelectable && isMouseReleased)
      isSelected = !isSelected;
  }

  public boolean isInside(float _pointx, float _pointy) {
    if (_pointx > x)
      if (_pointy > y)
        if (_pointx < x+w)
          if (_pointy < y+h)
            return true;

    return false;
  }
}
