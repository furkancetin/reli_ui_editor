public class Canvas extends WidgetArea {

  protected boolean isGridEnabled = true;
  protected boolean isGridVisible = true;
  protected int gridSize;


  //private WidgetArea widArea;

  public int snapToGridX(float _a) {
    return int(x) + int(floor((_a-x+gridSize*0.5f) / float(gridSize))*gridSize);
  }

  public int snapToGridY(float _a) {
    return int(y) + int(floor((_a-y+gridSize*0.5f) / float(gridSize))*gridSize);
  }

  public void snapToGrid(Area _area) {
    _area.x = snapToGridX(_area.x);
    _area.y = snapToGridY(_area.y);
  }


  Canvas() {
  }

  Canvas(Area _area) {
    setArea(_area);
    isGridEnabled = false;
    isGridVisible = false;
    gridSize = 32;
  }

  Canvas(Area _area, int _gridSize) {
    setArea(_area);
    gridSize = _gridSize;
  }

  public void addWidget(Widget _widget) {
    super.addWidget(_widget);

    if (isGridEnabled) {
      _widget.x = snapToGridX(_widget.x);
      _widget.y = snapToGridY(_widget.y);
    }
  }

  public int getGridSize() {
    return gridSize;
  }

  public void setGridSize(int _size) {
    gridSize = _size;
  }

  public void setGridEnabled(boolean _isEnabled) {
    isGridEnabled = _isEnabled;
  }

  public void setGridVisible(boolean _isVisible) {
    isGridVisible = _isVisible;
  }

  public void setWidgetsSelectable(boolean _isSelectable) {
    for (int i=0; i< list.size(); i++) {
      list.get(i).isSelectable = _isSelectable;
    }
  }

  public void setWidgetsEditable(boolean _isEditable) {
    for (int i=0; i< list.size(); i++) {
      list.get(i).isEditable = _isEditable;
    }
  }


  void render() {
    if (isGridVisible) {

      if (isGridEnabled)
        stroke(66, 66, 66);
      else
        stroke(22, 22, 22);

      strokeWeight(1);
      noFill();

      //translate(x, y);

      for (int i=0; i <= h/gridSize; i++) {
        line(x, y+i*gridSize, x+w, y+i*gridSize);
      }
      for (int i=0; i <= w/gridSize; i++) {
        line(x+i*gridSize, y, x+i*gridSize, y+h);
      }
    }

    super.render();
  }
}
