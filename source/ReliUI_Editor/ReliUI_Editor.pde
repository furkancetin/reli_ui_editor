
final int gridSize = 32;
final int topbartHeight = 85;
final int menuLeftWidth = 96;
PImage imageLogo;

Area areaPage ;
Canvas canvasEditor;
WidgetList menuLeft;
Button buttonClose;

boolean modeAddScalable = false;

public boolean isGlobalMouseReleased = false;
public boolean isGlobalMousePressed  = false;

enum PAGE {
  SETTINGS, EDITOR, TEST
}

PAGE pageNow = PAGE.SETTINGS;

void setup() {
  initMainCanvas();

  canvasEditor = new Canvas(areaPage, gridSize);
  menuLeft   = new WidgetList(0, topbartHeight);
  menuLeft.addWidget(new Button("menuSettings_normal.png", "menuSettings_hover.png", "menuSettings_hover.png"));
  menuLeft.addWidget(new Button("menuEdit_normal.png", "menuEdit_hover.png", "menuEdit_hover.png"));
  menuLeft.addWidget(new Button("menuTest_normal.png", "menuTest_hover.png", "menuTest_hover.png"));

  menuLeft.isDebugMode = false;
}

void draw() {
  drawMainCanvas();

  menuLeft.render();

  //Page Changing Mechanism
  int newPage = menuLeft.getMousePressedWidgetId();

  if (newPage == 0)
    gotoPage(PAGE.SETTINGS);
  else if (newPage == 1)
    gotoPage(PAGE.EDITOR);
  else if (newPage == 2)
    gotoPage(PAGE.TEST);


  //Drawing Page Contents
  if (pageNow == PAGE.SETTINGS) {
  } else if (pageNow == PAGE.EDITOR) {
    canvasEditor.render();

    if (modeAddScalable && canvasEditor.isMouseHover) {
      if (canvasEditor.getMouseHoverWidgetId() == -1) {
        stroke(0, 255, 0);
        fill(155, 155, 155, 155);

        if (canvasEditor.isMouseReleased) {
          canvasEditor.addWidget(new Scalable(canvasEditor, new Area(mouseX-64, mouseY-48, 128, 96) ));
        }
      } else {
        stroke(255, 0, 0);
        fill(155, 155, 155, 155);
      }

      if (canvasEditor.isMouseReleased) {
        modeAddScalable = false;
      }

      rect(canvasEditor.snapToGridX(mouseX-64), canvasEditor.snapToGridY(mouseY-48), 128, 96);
    }
  } else if (pageNow == PAGE.TEST) {

    canvasEditor.render();
  }

  isGlobalMouseReleased = false;
  isGlobalMousePressed  = false;
}

void gotoPage(PAGE _newpage) {
  if (_newpage == pageNow)
    return;

  if (_newpage == PAGE.SETTINGS) {
  } else  if (_newpage == PAGE.EDITOR) {
    canvasEditor.setWidgetsEditable(true);
    canvasEditor.setGridVisible(true);
  } else if (_newpage == PAGE.TEST) {
    canvasEditor.setWidgetsEditable(false);
    canvasEditor.setGridVisible(false);
  }

  pageNow = _newpage;
}

void keyPressed() {
  if (key == 'A' || key == 'a') {
    modeAddScalable = !modeAddScalable;
  }
}

void mouseReleased() {
  isGlobalMouseReleased = true;
}

void mousePressed() {
  if (mousePressed)
    isGlobalMousePressed = true;
}
