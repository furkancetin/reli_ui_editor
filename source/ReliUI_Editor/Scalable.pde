public class Scalable extends Widget {

  private boolean isMoving = false;
  public Canvas parentCanvas = null;

  Scalable() {
  }

  Scalable(Canvas _parentCanvas, Area _area) {
    parentCanvas = _parentCanvas;
    setArea(_area);
  }

  void render() {
    updateArea();

    if (isEditable) {
      if (isMousePressed)
        isMoving = true;
      else if (isMouseReleased) {
        isMoving = false;
        x = parentCanvas.snapToGridX(x);
        y = parentCanvas.snapToGridY(y);
      }

      if (isMoving == true) {
        x += mouseX-pmouseX;
        y += mouseY-pmouseY;

        stroke(122, 144, 200);
        noFill();
        rect(parentCanvas.snapToGridX(x), parentCanvas.snapToGridY(y), 128, 96);
      }
    }

    if (isMouseHover) {
      stroke(67, 189, 67);
      fill(195);
    } else if (isSelected) {
      stroke(0, 255, 0);
      fill(185);
    } else {
      stroke(120);
      fill(155);
    }


    strokeWeight(2); 
    rect(x, y, w, h);
    strokeWeight(1);

    if (isEditable && isSelected) {
      rectMode(RADIUS);
      rect(x, y, 5, 5);
      rect(x, y+h, 5, 5);
      rect(x+w, y+h, 5, 5);
      rect(x+w, y, 5, 5);

      rectMode(CORNER);
    }
  }
}
