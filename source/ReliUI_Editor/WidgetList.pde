class WidgetList extends Widget {
  protected ArrayList<Widget> list = new ArrayList<Widget>();
  protected boolean isVertical = true;
  protected boolean isMultipleSelectable = false;

  WidgetList() {
    setArea(0, 0, 0, 0);
  }

  WidgetList(float _x, float _y) {
    setArea(_x, _y, 0, 0);
  }

  WidgetList(float _x, float _y, boolean _isVertical) {
    isVertical = _isVertical;
    setArea(_x, _y, 0, 0);
  }

  public void addWidget(Widget _widget) {
    list.add(_widget);
    updateAreas();
  }

  public Widget getWidget(int i) {
    return list.get(i);
  }

  public int getWidgetSize() {
    return list.size();
  }

  public void removeWidget(int i) {
    list.remove(i);
    updateAreas();
  }

  public void render() {
    updateArea();

    if (!isMultipleSelectable && isMousePressed)
      for (int i=0; i< list.size(); i++) {
        list.get(i).isSelected = false;
      }

    for (int i=0; i< list.size(); i++) {
      list.get(i).render();

      if (isDebugMode)
        list.get(i).renderArea();
    }

    if (isDebugMode)
      renderArea();
  }

  public void setVertical(boolean _isVertical) {
    isVertical = _isVertical;
  }

  public Widget getMouseHoverWidget() {
    for (int i=0; i< list.size(); i++) {
      Widget ww = list.get(i);
      if (ww.isMousePressed)
        return ww;
    }
    return null;
  }

  public int getMouseHoverWidgetId() {
    for (int i=0; i< list.size(); i++) {
      Widget ww = list.get(i);
      if (ww.isMousePressed)
        return i;
    }
    return -1;
  }

  public Widget getMousePressedWidget() {
    if (mousePressed)
      return getMouseHoverWidget();
    else
      return null;
  }

  public int getMousePressedWidgetId() {
    if (mousePressed)
      return getMouseHoverWidgetId();
    else
      return -1;
  }

  public void updateAreas() {
    h = 0;
    w = 0;

    for (int i=0; i< list.size(); i++) {
      Widget ww = list.get(i);
      ww.isDebugMode = isDebugMode;

      if (isVertical) { //In VERTICAL alignment, all x values are same, y changes incrementally
        ww.x = x;
        ww.y = y+h;
        h += ww.h;
        w = max(ww.w, w);
      } else {         //In HORIZONTAL alignment, all x values are same, y changes incrementally
        ww.x = x+w;
        ww.y = y;
        w += ww.w;
        h = max(ww.h, h);
      }
    }
  }
}
