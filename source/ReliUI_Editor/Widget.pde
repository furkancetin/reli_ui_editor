abstract class Widget extends Area {

  public boolean isDebugMode = false;
  public Widget parent = null;

  protected boolean isVisible = true;
  protected boolean isEnabled = true;
  protected boolean isEditable = true;


  Widget() {
  }

  public abstract void render();

  public void setEnabled(boolean _isEnabled) {
    isEnabled = _isEnabled;
  }

  public void setVisible(boolean _isVisible) {
    isVisible = _isVisible;
  }

  public void setMovable(boolean _isEditable) {
    isEditable = _isEditable;
  }
}
