void settings() {
  fullScreen();
}

void initMainCanvas() {
  surface.setSize(1920, 1040);
  surface.setResizable(true);
  surface.setLocation(0, 0);
  
  areaPage = new Area(menuLeftWidth, topbartHeight, width-menuLeftWidth, height-topbartHeight);
  
  frameRate(60);
  textSize(18);

  imageLogo = loadImage("logo_reli.png");
  buttonClose = new Button(1854, 21, "icon_close_normal.png", "icon_close_hover.png", "icon_close_pressed.png");
}

void drawMainCanvas() {



  background(33, 36, 40);

  noStroke();
  fill(0, 0, 0);
  rect(0, 0, width, topbartHeight);
  fill(25, 28, 32);
  rect(0, 85, 96, height-topbartHeight);
  image(imageLogo, 24, 24);

  buttonClose.render();
  
  fill(255);
  //text(frameRate,1,20);  //Displays FPS

  if (buttonClose.isMousePressed)
    exit();
}
