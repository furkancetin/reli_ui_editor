class WidgetArea extends Widget {
  protected ArrayList<Widget> list = new ArrayList<Widget>();
  protected boolean isMultipleSelectable = false;

  WidgetArea() {
    setArea(0, 0, 0, 0);
  }

  WidgetArea(Area _area) {
    setArea(_area);
  }

  public void addWidget(Widget _widget) {
    list.add(_widget);
  }

  public Widget getWidget(int i) {
    return list.get(i);
  }

  public int getWidgetSize() {
    return list.size();
  }

  public void removeWidget(int i) {
    list.remove(i);
  }

  public void render() {
    updateArea();

    if (!isMultipleSelectable && isMousePressed)
      for (int i=0; i< list.size(); i++) {
        list.get(i).isSelected = false;
      }

    for (int i=0; i< list.size(); i++) {
      list.get(i).render();
    }

    if (isDebugMode)
      renderArea();
  }

  public Widget getMouseHoverWidget() {
    for (int i=0; i< list.size(); i++) {
      Widget ww = list.get(i);
      if (ww.isMouseHover)
        return ww;
    }
    return null;
  }

  public int getMouseHoverWidgetId() {
    for (int i=0; i< list.size(); i++) {
      Widget ww = list.get(i);
      if (ww.isMouseHover)
        return i;
    }
    return -1;
  }

  public Widget getMousePressedWidget() {
    if (mousePressed)
      return getMouseHoverWidget();
    else
      return null;
  }

  public int getMousePressedWidgetId() {
    if (mousePressed)
      return getMouseHoverWidgetId();
    else
      return -1;
  }
}
