public class Button extends Widget {

  protected PImage imgNormal;
  protected PImage imgHover;
  protected PImage imgPressed;
  protected PImage imgDisabled;

  Button() {
  }

  Button(float _x, float _y, String _pathNormal, String _pathHover, String _pathPressed, String _pathDisabled) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathHover);
    imgPressed = loadImage(_pathPressed);
    imgDisabled = loadImage(_pathDisabled);

    setArea(_x, _y, imgNormal.width, imgNormal.height);
  }

  Button(float _x, float _y, String _pathNormal, String _pathHover, String _pathPressed) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathHover);
    imgPressed = loadImage(_pathPressed);
    imgDisabled = loadImage(_pathNormal);

    setArea(_x, _y, imgNormal.width, imgNormal.height);
  }

  Button(float _x, float _y, String _pathNormal) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathNormal);
    imgPressed = loadImage(_pathNormal);
    imgDisabled = loadImage(_pathNormal);

    setArea(_x, _y, imgNormal.width, imgNormal.height);
  }

  Button(String _pathNormal) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathNormal);
    imgPressed = loadImage(_pathNormal);
    imgDisabled = loadImage(_pathNormal);

    setArea(0, 0, imgNormal.width, imgNormal.height);
  }

  Button(String _pathNormal, String _pathHover, String _pathPressed, String _pathDisabled) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathHover);
    imgPressed = loadImage(_pathPressed);
    imgDisabled = loadImage(_pathDisabled);

    setArea(0, 0, imgNormal.width, imgNormal.height);
  }

  Button(String _pathNormal, String _pathHover, String _pathPressed) {
    imgNormal = loadImage(_pathNormal);
    imgHover = loadImage(_pathHover);
    imgPressed = loadImage(_pathPressed);
    imgDisabled = loadImage(_pathNormal);

    setArea(0, 0, imgNormal.width, imgNormal.height);
  }

  public void setSelectable(boolean _isSelectable) {
    isSelectable = _isSelectable;
  }



  public void render() {
    updateArea();

    if (isVisible) {
      if (!isEnabled)
        image(imgDisabled, x, y);
      else if (isMousePressing)
        image(imgPressed, x, y);
      else if (isMouseHover || (isSelectable && isSelected)) //TODO: Needs another image to display (imgSelected)
        image(imgHover, x, y);
      else
        image(imgNormal, x, y);
    }

    if (isDebugMode)
      renderArea();
  }
}
